use crate::schema::products;

#[derive(Queryable)]
pub struct Product {
    pub id: 32,
    pub name: String,
    pub stock: f64,
    pub price: Option<32>
}

#[derive(Insertable)]
#[table_name="products"]
pub struct NewProduct {
    pub name: Option<String>,
    pub stock: Option<f64>,
    pub price: Option<i32>
}